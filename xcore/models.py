from gpj.users.models import *
# Create your models here.

class Personne(Model):
    perso = (
        ("PHY", "physique"),
        ("MOR", "morale")
    )
    sexo = (
        ("H", "Homme"),
        ("F", "Femme")
    )
    nature = CharField(max_length=3, choices=perso)
    pm = TextField()
    infos_sup = TextField()
    nom = CharField(max_length=255)
    prenoms = CharField(max_length=255)
    adresse = CharField(max_length=255, blank=True)
    situation_matrimoniale = CharField(max_length=255)
    sexe = CharField(max_length=1, choices=sexo)
    profession = CharField()


class Denonciation(Model):
    victime = OneToOneField(Personne, on_delete=CASCADE)
    mis_en_cause = OneToOneField(Personne, on_delete=CASCADE)
    pv = TextField()
    image = ImageField(upload_to="files", null=True)
    video = FileField(upload_to="files", null=True)
    audio = FileField(upload_to="files", null=True)

class Plainte(Model):
    choix = (
        ("ATT", "En attente"),
        ("REJ", "Rejetée"),
        ("RES", "Résolue")
    )
    plaignant = OneToOneField(User, on_delete=CASCADE)
    victime = OneToOneField(Personne, on_delete=CASCADE)
    mis_en_cause = OneToOneField(Personne, on_delete=CASCADE)
    relation = TextField(default=" Non renseigné ")
    objet = TextField()
    pv = TextField()
    statut = CharField(max_length=3, choices=choix, default="ATT")

