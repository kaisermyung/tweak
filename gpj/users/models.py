from django.contrib.auth.models import AbstractUser
from django.db.models import *
from django.urls import reverse
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    """Default user for GPJ.
    """

    #: First and last name do not cover name patterns around the globe
    name = CharField(_("Name of User"), blank=True, max_length=255)
    email = EmailField(unique=True)
    nature = CharField(max_length=200, default="")
    telephone = CharField(max_length=8, blank=True, null=True)
    sexe = CharField(max_length=5, null=True, blank=True)
    #profession = CharField(max_length=255, blank=True, null=True)
    def get_absolute_url(self):
        """Get url for user's detail view.

        Returns:
            str: URL for user detail.

        """
        return reverse("users:detail", kwargs={"username": self.username})


class Agent(Model):
    user = OneToOneField(User, on_delete=CASCADE)
    commissariat = CharField(max_length=255)


class Avocat(Model):
    priv = (
        ("u", "Unique"),
        ("A", "Anybody")
    )
    state = (
        ("O", "Occuppé"),
        ("L", "Libre")
    )
    user = OneToOneField(User, on_delete=CASCADE)
    ci = FileField(upload_to='files')
    doc = FileField(upload_to='files')
    speciality = CharField(max_length=255)
    privacy = CharField(max_length=8, choices=priv, default="A")
    statut = CharField(max_length=1, choices=state, default="L")


